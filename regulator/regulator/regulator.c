/**
 * @file regulator.c
 */
#include <machine.h>
#include "iodefine.h"
#include "typedefine.h"
#include "pid.h"
#include "LCD_Display.h"

volatile unsigned char adResult;
/// Struktura danych używanego w programie regulatora PID
PID_Struct pidInst;

/**
 * Funkcja main.
 *
 * Wykonuje inicjalizacje, które nie zostały zrobione w hwsetup, włącza przerwania 
 * i przechodzi w pętlę nieskończoną podtrzymującą działanie programu. Reszta wykonuje się w przerwaniach.
*/
void main(void)
{	
	PID_Init(&pidInst);
	LCD_Init();
	
	set_imask_ccr(0);           // enable all interrupts
	
	while(1)
	{
	}
}
