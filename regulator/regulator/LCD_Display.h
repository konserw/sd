/*******************************************************************************
 *
 *  Module:         LCD_Dispaly.h
 *
 *  Description:    Module to access the 2 x 16 character LCD-display.
 *
 *  Compiler:       H8S, H8/300 Toolchain Version 6.1.2.0
 *
 *  Processors:     H8/36077,36079 on ModSDKH8-CH8T36077/36079
 *
 *  Author(s):      Maik Otto
 *
 *  Company:        Delphin electronic GmbH, Mühlhausen/Thüringen (Germany)
 *                  info@delphin-electronic.de
 *
 *  $Id: LCD_Display.h 2 2007-08-23 11:15:18Z Maik $
 *
 ******************************************************************************/

#ifndef LCD_DISPLAY_HEADER_FIRST_INCLUDE_GUARD
#define LCD_DISPLAY_HEADER_FIRST_INCLUDE_GUARD

#include "typedefine.h"

// We need this external variable to remember the
// value of the last write-access to PCR3
// This variable is instanciated in CPU_Init.c
// and is initialized with 0x00
extern unsigned char PCR3_Value;


// Initialize the disply
void LCD_Init(void);

/* Write a string to a given Line start at a given position. If the string is
   smaller than 16 it must be 0-terminated */
void LCD_Write_String(unsigned char LineNumber, unsigned char StartPos, const unsigned char* Text);

// Clear the display
void LCD_Clear_Display(void);

// Set the cursor mode to on/off and blinking/not-blinking
void LCD_Set_Cursor_Mode(_Bool On, _Bool Blinking);

// Move the cursor to home
void LCD_Set_Cursor_Home(void);


#endif /* LCD_DISPLAY_HEADER_FIRST_INCLUDE_GUARD */
