#include "pid.h"

#define _kp 50.0
#define _ki 0.01
#define _kd 10.0
#define Tp 0.5
#define initialSP 0.0;

void PID_Init(PID_Struct* str)
{
	str -> ki = _ki;
	str -> kd = _kd;
	str -> kp = _kp;
	str -> tp = Tp;
	str -> err_prev = 0;
	str -> integral = 0;
	str -> sp = initialSP;
}

void PID_UpdateParams(PID_Struct* str, float kp, float ki, float kd)
{
	str -> kp = kp;
	str -> kd = kd;
	str -> ki = ki;
}

void PID_UpdateSetpoint(PID_Struct* str,  float value)
{
	str -> sp = value;
}

float PID_Sim(PID_Struct* str, float pv)
{
	float err = str -> sp - pv;
	float out;

	/*out = str -> q0 * err;
	out += str -> q1 * str -> err_prev;
	out += str -> q2 * str -> err_prev2;
	out += str -> out_prev;

	out = (out > 100.0 ? 100.0 : out);
	out = (out < 0 ? 0 : out);

	str -> err_prev2 = str -> err_prev;
	str -> err_prev = err;
	str -> out_prev = out;*/
	
	str -> integral += err * str -> ki * str -> tp;
	if(str -> integral >= 100.0)
		str -> integral = 100.0;
	else if(str -> integral <= 0.0)
		str -> integral = 0;
	
	out = str -> kp * err + (err - str -> err_prev) * str -> kd / str -> tp + str -> integral;
	
	out = (out > 100.0 ? 100.0 : out);
	out = (out < 0 ? 0 : out);

	return out;
}