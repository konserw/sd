/**
 * @file pid.h
 */
#ifndef PID_H
#define PID_H

#include "typedefine.h"

/**
 * Struktura przechowująca parametry i historię regulatora PID
 */
typedef struct
{
	float kp;
	float ki;
	float kd;
	float tp;
	float err_prev;
	float integral;
	float sp;
}PID_Struct;

/**
 * @brief Funkcja zmieniająca parametry regulatora. Należy to robić wyłącznie za jej pośrednictwem ze względu na konieczność przeliczenia.
 * @param str Wskaźnik na strukturę PID
 * @param kp Wzmocnienie proporcjonalne
 * @param ki Wzmocnienie części całkowej
 * @param kd Wzmocnienie części różniczkującej
 */
void PID_UpdateParams(PID_Struct* str, float kp, float ki, float kd);

/**
 * @brief Funkcja zmieniająca wartość zadaną w regulatorze PID.
 * @param value Nowa wartosc
 */
void PID_UpdateSetpoint(PID_Struct* str,  float value);

/**
 * @brief Funkcja wykonująca jeden krok symulacji regulatora
 * @param str Wskaźnik na strukturę PID, który ma być symulowany
 * @param pv Wartość wyjścia procesu
 * @return Wyjście regulatora
 */
float PID_Sim(PID_Struct* str, float pv);

/**
 * @brief Inicjalizacja struktury
 * @param str Wskaźnik na strukturę PID
 */
void PID_Init(PID_Struct* str);

#endif