/**
 * @file uart.h
 */
#ifndef UART_H
#define UART_H

#include "typedefine.h"

/**
 * Struktura przechowująca zmienne potrzebne do obsłgi komunikacji przez UART.
 */
typedef struct
{
	struct st_sci3* sci;
	char sendBuffer[20];
	char receiveBuffer[20];
	_UBYTE sent;
	_UBYTE toSend;
	_UBYTE received;
}UartStruct;

/**
 * Inicjalizacja struktur
 */
void UartInit();

/**
 * Obsługa przerwania uruchomionego przy wysłaniu znaku, wspólna dla obu połączeń. Potrzebna do obsługi wysłania ciągu znaków.
 * @param Struktura przechowująca dane do wysłania i dodatkowe zmienne
 */
void SendInterruptHandler(UartStruct*);

/**
 * Obsługa odbioru znaku od komputera
 * @param rcv Odebrany znak
 */
void pcReceiveHandler(char rcv);

/**
 * Obsługa odbioru znaku od obiektu
 * @param rcv Odebrany znak
 */
void objectReceiveHandler(char rcv);

/**
 * Wysłanie pojedynczej wartości.
 * @param str Struktura UART przechowująca dane przesyłu
 * @param value Wartość do przesłania
 */
void SendU(UartStruct* str, _UWORD value);

/**
 * Wysłanie ramki z trzema wartościami, używane do wizualizacji
 * @param str Struktura UART przechowująca dane przesyłu
 * @param sp Wartość zadana
 * @param pv Wartość wyjścia obiektu
 */
void SendValues(UartStruct* str, _UWORD sp, _UWORD pv, _UWORD u);

#endif