/**
 * @file uart.c
 */
#include "uart.h"
#include "pid.h"
#include "stdlib.h"
#include "iodefine.h"
#include "LCD_Display.h"

UartStruct objectConn;
UartStruct pcConn;

extern PID_Struct pidInst;
extern float pv;

/**
 * Przetworzenie 16bitowej zmiennej całkowitej na ciąg 4 znaków w zapisie szesnastkowym
 * @param buf Miejsce na wynikowy łańcuch znaków
 * @param value Wartość do przetworzenia
 */
void IntToHex(char* buf, _UWORD value)
{
	_UWORD val = value;
	_UBYTE i;
	buf[3] = (val & 0x000f) + 48;
	val = val>>4;
	buf[2] = (val & 0x000f) + 48;
	val = val>>4;
	buf[1] = (val & 0x000f) + 48;
	val = val>>4;
	buf[0] = (val & 0x000f) + 48;
	
	for(i = 0; i<4; ++i)
	{
		if(buf[i] > 57)
			buf[i] += 7;
	}
}

/**
 * Przetworzenie zapisu szesnastkowego na liczbę całkowitA Funkcja zakłada stały rozmiar napisu: 4 znaki.
 * @param buf łańcuch znaków zawierający czteroznakową liczbę szesnastkową
 * @return Odczytana wartość
 */
_UWORD HexToInt(char* buf)
{
	_UWORD val = 0;
	
	val = buf[0] > 57 ? (buf[0] - 55) & 0x000f :  (buf[0] - 48) & 0x000f;
	val = val << 4;
	val |= buf[1] > 57 ? (buf[1] - 55) & 0x000f :  (buf[1] - 48) & 0x000f;
	val = val << 4;
	val |= buf[2] > 57 ? (buf[2] - 55) & 0x000f :  (buf[2] - 48) & 0x000f;
	val = val << 4;
	val |= buf[3] > 57 ? (buf[3] - 55) & 0x000f :  (buf[3] - 48) & 0x000f;
	
	return val;
}
	
void UartInit()
{
	pcConn.sci = &SCI3;
	objectConn.sci = &SCI3_2;
}

/**
 * Liczby przesyłane są w zapisie szesnastkowym. Stosujemy liczby 16bitowe, stąd na każdą wartość przypadają 4 znaki.
 * Ramkę kończy znak końca linii.
 */
void SendU(UartStruct* str, _UWORD value)
{
	IntToHex(str->sendBuffer, value);
	str->sendBuffer[4] = '\n';
	str->toSend = 5;
	str->sent = 0;
	str->sci->TDR = str->sendBuffer[0];
	str->sci->SCR3.BYTE |= 0x80; // Włączenie przerwania
}

/**
 * W przypadku tej funkcji ramka składa się z trzech liczb szesnastkowych tworzących ciągły napis zakończony znakiem nowej linii.
 */
void SendValues(UartStruct* str, _UWORD sp, _UWORD pv, _UWORD u)
{
	IntToHex(str->sendBuffer, sp);
	IntToHex(&(str->sendBuffer[4]), pv);
	IntToHex(&(str->sendBuffer[8]), u);
	str->sendBuffer[12] = '\n';
	str->toSend = 13;
	str->sent = 0;
	str->sci->TDR = str->sendBuffer[0];
	 // W��czenie przerwania - domy�lnie flaga jest w��czona i wy��czana dopiero po zapisie do bufora,
	 // dlatego jest uruchamiane dopiero teraz, aby nie utkn�� w obs�udze przerwania
	str->sci->SCR3.BYTE |= 0x80;
}

void SendInterruptHandler(UartStruct* str)
{
	++(str->sent);
	--(str->toSend);
	if(str->toSend > 0)
	{
		(str->sci)->TDR = str->sendBuffer[str->sent];
	}
	else
	{
		str->sci->SCR3.BYTE &= 0x7F; // Wy��czenie przerwania - nie ma ju� mo�liwo�ci wyczyszczenia flagi, wi�c nale�y wy��czy� przerwanie.
	}
}

void pcReceiveHandler(char rcv)
{
	if(rcv != '\n')
	{
		pcConn.receiveBuffer[pcConn.received] = rcv;
		++(pcConn.received);
	}
	else
	{		
		if(pcConn.received == 4)
		{// pojedyncza wartość
			//PID_UpdateSetpoint(&pidInst, ((float)HexToInt(pcConn.receiveBuffer) / 100.0));
			pidInst.sp = (float)HexToInt(pcConn.receiveBuffer) / 100.0;
			IO.PDR5.BYTE = 0x01;
		}
		else if(pcConn.received == 12)
		{// 3 warto�ci
			float kp = (float)HexToInt(pcConn.receiveBuffer) / 100.0;
			float ki = (float)HexToInt(&pcConn.receiveBuffer[4]) / 100.0;
			float kd = (float)HexToInt(&pcConn.receiveBuffer[8]) / 100.0;
			PID_UpdateParams(&pidInst, kp, ki, kd);
			
		LCD_Write_String(0, 0, pcConn.receiveBuffer);
		}
		pcConn.received = 0;
	}
}

void objectReceiveHandler(char rcv)
{
	if(rcv != '\n')
	{
		objectConn.receiveBuffer[objectConn.received] = rcv;
		++(objectConn.received);
	}
	else
	{
		IO.PDR5.BYTE ^= 0x02;
		pv = HexToInt(objectConn.receiveBuffer) / 100.0;
		objectConn.received = 0;
	}
}