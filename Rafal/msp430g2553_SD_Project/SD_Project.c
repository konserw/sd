
#include <msp430.h>
#include <stdlib.h>
#include <math.h>

#define pi 3.14159							// Definicja sta�ej PI

/**
* @brief Zmienne odpowiedzialne za transmisje
*/
char RBUFOR[6];								/**< Bufor na odebrana wiadomosc*/
volatile unsigned int tit = 0;				/**< Iterator dla TBUFOR*/
char *TBUFOR;								/**< Bufor na wysylana wiadomosc*/
volatile unsigned int rit = 0;				/**< Iterator dla RBUFOR*/
volatile unsigned int tempr = 0;			/**< Zmienna tymczasowa*/
volatile char flagr = 0;					/**< Flaga odebrania wiadomosci*/

/**
* @brief Zmienne wejcia i wyjscia
*/
volatile unsigned int we = 0;				/**< Wejscie obiektu w postaci int*/
volatile unsigned int wy = 0;				/**< Wyjscie obiektu w postaci int*/

/**
* @brief Zmienne wewnetrzne obiektu
*/
volatile float Vwe = 0;						/**< Przeplyw wejsciowy*/
volatile float h = 0.00001;					/**< Wysokosc cieczy w zbiorniku*/
volatile float Vzadane = 30;				/**< Wartosc zadana obiektu*/



/**
* @brief Funkcja konwertujaca int -> string hex
*/
char* itoa(int wart);						/**<Funkcja konwertujaca int -> string hex*/

/**
* @brief Funkcja konwertujaca string hex -> int
*/
unsigned int atoid(char *str);				/**<Funkcja konwertujaca string hex -> int*/




int main(void)
{
  WDTCTL = WDTPW + WDTHOLD;                 /**< Wylaczenie watchdog'a*/

  /**
  * @brief KONFIG. LEDOW
  */
  P1DIR |= (BIT0|BIT6); 					/**< Konfiguracja wyjsc diod na plytce*/
  P1OUT = 0x00;  							/**< Wylaczenie diod na plytce*/

  /**
  * @brief KONFIG. TIMERA
  */
  CCTL0 = CCIE;                             /**< Wl. przerwan generowanych przez timer*/
  CCR0 = 62500;								/**< Ustawienie wartosci granicznej timera (okres 500ms)*/
  TACTL = TASSEL_2 + ID_3 + MC_1;           /**< Zegar wew. SMCLK, dzielnik 8, tryb Up Mode*/

  /**
  * @brief KONFIG. USART
  */
  BCSCTL1 = CALBC1_1MHZ; 					/**< Konfiguracje zagarow*/
  DCOCTL = CALDCO_1MHZ;						/**< Konfiguracje zagarow*/
  P1DIR = BIT0 + BIT6;						/**< Konfiguracje pinow TX/RX*/
  P1SEL = BIT1 + BIT2 ;                     /**< Ustawienie ich na funkcje alternatywna*/
  P1SEL2 = BIT1 + BIT2; 					/**< Ustawienie ich na funkcje alternatywna*/

  UCA0CTL1 |= UCSSEL_2;                     /**< Zegar wew. SMCLK*/
  UCA0BR0 = 104;                            /**< Ustawienie predkosci transmisji na 9600*/
  UCA0BR1 = 0;                              /**< Ustawienie predkosci transmisji na 9600*/
  UCA0MCTL = UCBRS0;               			/**< Modulation UCBRSx = 3*/
  UCA0CTL1 &= ~UCSWRST;                     /**< Initialize USCI state machine*/
  IE2 |= UCA0RXIE;               			/**< Wl. przerwan od przychodzacych wiadomosci*/

  __bis_SR_register(LPM0_bits + GIE);       /**< Enter LPM3 w/ interrupts enabled*/

  while(1){}

}





//################################################################################
//                        PROCEDURA PRZERWANIA WYSYLANIA
//################################################################################

/**
* @brief OBSLUGA PRZERWANIA WYSYLANIA
*/
#pragma vector=USCIAB0TX_VECTOR
__interrupt void USCI0TX_ISR(void)
{
	TBUFOR[4]='\n';							/**< Dodanie znaku konca ramki do wiadomosci*/

	UCA0TXBUF = TBUFOR[tit++];              /**< Transmisja kolejnych znakow z bufora*/

	if (tit >= 5 )                   		/**< Warunek konca transmisji - wyslano 5 znakow*/
	{IE2 &= ~UCA0TXIE; tit=0;				/**< Wylaczenie przerwan wysylania*/
	P1OUT ^= 0x40;	}
}

//################################################################################
//                        PROCEDURA PRZERWANIA ODBIERANIA
//################################################################################

/**
* @brief OBSLUGA PRZERWANIA ODBIERANIA
*/
#pragma vector=USCIAB0RX_VECTOR
__interrupt void USCI0RX_ISR(void)
{

	RBUFOR[rit++]=UCA0RXBUF;				/**< Odbieranie kolejnych znakow i wpisywanie ich do bufora*/

	if( (RBUFOR[rit-1]==0x0A) && (rit==5) )	/**< Warunek konca odbierania / znak konca ramki / pelny bufor*/
	{rit = 0; flagr = 1;}
	else if((RBUFOR[rit-1]==0x0A)&&(rit<5)) /**< Niepelna ramka, za malo znakow*/
	{rit = 0;}
	if (rit>5)								/**< Brak konca znaku w ramce, za duzo znakow*/
	{rit = 0;}

}



//################################################################################
//            PROCEDURA PRZERWANIA TIMERA - OBLICZ. WYJSCIA OBIEKTU
//################################################################################

/**
* @brief PROCEDURA PRZERWANIA TIMERA - OBLICZANIE WYJSCIA OBIEKTU
*/
#pragma vector=TIMER0_A0_VECTOR
__interrupt void Timer_A (void)
{

	float x;								/**< Tymczasowa zmienna wewnetrzna obietu*/

	if(flagr==1)							/**< Jesli nadeszla nowa wiadomosc to przetworzenie jej na int*/
	{
		P1OUT ^= 0x01;						/**< Sygnalizacja odebrania przelaczeniem diody*/
		IE2 &= ~UCA0RXIE;					/**< Wylaczenie przerwan od przychodzacych wiadomosci*/
		we = atoid(RBUFOR);					/**< Konwersja odebranej wiadomosci na wartosc int*/
		flagr = 0;							/**< Zerowanie flagi odbioru*/
		IE2 |= UCA0RXIE;					/**< Wlaczenie przerwan od przychodzacych wiadomosci*/
	}

	Vzadane = we*0.03;						/**< Przeliczenie odebranej wartosci na Vzadane*/

	Vwe=Vwe+0.1*((Vzadane-Vwe)/10.0);		/**< Obliczenie wyjscia pompy*/

	if(h<25)
	{
	     if(h<0.1)
	     {
	    	 h=h+((Vwe-5*sqrt(h))*0.051741);
	     }
	     else
	     {
	    	 h=h+0.1*((Vwe-5*sqrt(h))/(157.079633*h-pi*h*h));
	     }
	}
	else
	{
	    if(h>=49.9)
	    {
	    	x=50-h;
	        x=x+((Vwe-5*sqrt(50-x))*(-0.051741));
	        h=50-x;
	    }
	    else
	    {
	        x=50-h;
	        x=x+0.1*((Vwe-5*sqrt(50-x))/(pi*x*x-157.079633*x));
	        h=50-x;
	    }
	}

	if(h>50){ h=49.9999; }
	if(h<=0){ h=0.00001; }

	wy = (unsigned int)(h*200);				/**< Przeliczenie wart. poziomu na int*/
	TBUFOR = itoa(wy);						/**< Konwersja na hexa*/
	IE2 |= UCA0TXIE;              			/**< Wl. przerwan wysylania wiadomosci*/
}



//################################################################################
//               FUNKCJA KONWERSJI ZMIENNEJ INT -> CHAR[HEX]
//################################################################################

/**
* @brief FUNKCJA KONWERSJI ZMIENNEJ INT -> STRING HEX
*/
char* itoa(int wart)
{
	static char buf[6] = {NULL};

	int i = 4;

	for(   ; wart||i ; --i , wart/=16)
		{buf[i] = "0123456789ABCDEF"[wart % 16];}

	return &buf[i+1];
}


//################################################################################
//               FUNKCJA KONWERSJI ZMIENNEJ CHAR[HEX] -> INT
//################################################################################

/**
* @brief FUNKCJA KONWERSJI ZMIENNEJ STRING HEX -> INT
*/
unsigned int atoid(char *str)
{
	unsigned int wartosc = 0;
	unsigned int bufo = 0;
	int i = 0;

	const int tab[4]={4096,256,16,1};

	for(i=0;i<(strlen(str)-1);i++)
	{
		// Jezeli znakiem jest duza litera A-F
		if( ( ((int)str[i])>64 )&&( ((int)str[i])<71 ) )
		{
			bufo = ((int)str[i])-55;
		}
		// Jezeli znakiem jest cyfra 0-9
		else if ( ( ((int)str[i])>47 )&&( ((int)str[i])<58 ) )
		{
			bufo = ((int)str[i])-48;
		}

		// Obliczenie wartosci
		wartosc += tab[i]*bufo;
	}

	return wartosc;
}

//################################################################################

