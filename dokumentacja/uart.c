/**
 * @file uart.c
 */
#include "uart.h"
#include "pid.h"
#include "stdlib.h"
#include "iodefine.h"
#include "LCD_Display.h"

UartStruct objectConn;
UartStruct pcConn;

extern PID_Struct pidInst;
extern float pv;

/**
 * Przetworzenie 16bitowej zmiennej calkowitej na ciag 4 znakow w zapisie szesnastkowym
 * @param buf Miejsce na wynikowy lancuch znakow
 * @param value Wartosc do przetworzenia
 */
void IntToHex(char* buf, _UWORD value)
{
	_UWORD val = value;
	_UBYTE i;
	buf[3] = (val & 0x000f) + 48;
	val = val>>4;
	buf[2] = (val & 0x000f) + 48;
	val = val>>4;
	buf[1] = (val & 0x000f) + 48;
	val = val>>4;
	buf[0] = (val & 0x000f) + 48;
	
	for(i = 0; i<4; ++i)
	{
		if(buf[i] > 57)
			buf[i] += 7;
	}
}

/**
 * Przetworzenie zapisu szesnastkowego na liczbe calkowita. Funkcja zaklada staly rozmiar napisu: 4 znaki.
 * @param buf Lancuch znakow zawierajacy czteroznakowa liczbe szesnastkowa
 * @return Odczytana wartosc
 */
_UWORD HexToInt(char* buf)
{
	_UWORD val = 0;
	
	val = buf[0] > 57 ? (buf[0] - 55) & 0x000f :  (buf[0] - 48) & 0x000f;
	val = val << 4;
	val |= buf[1] > 57 ? (buf[1] - 55) & 0x000f :  (buf[1] - 48) & 0x000f;
	val = val << 4;
	val |= buf[2] > 57 ? (buf[2] - 55) & 0x000f :  (buf[2] - 48) & 0x000f;
	val = val << 4;
	val |= buf[3] > 57 ? (buf[3] - 55) & 0x000f :  (buf[3] - 48) & 0x000f;
	
	return val;
}
	
void UartInit()
{
	pcConn.sci = &SCI3;
	objectConn.sci = &SCI3_2;
}

/**
 * Liczby przesylane sa w zapisie szesnastkowym. Stosujemy liczby 16bitowe, stad na kazda wartosc przypadaja 4 znaki.
 * Ramke konczy znak konca linii.
 * @param str wskaźnik na strukturę ramki do uzupełnienia
 * @param value wartosc wpisywana do ramki
 */
void SendU(UartStruct* str, _UWORD value)
{
	IntToHex(str->sendBuffer, value);
	str->sendBuffer[4] = '\n';
	str->toSend = 5;
	str->sent = 0;
	str->sci->TDR = str->sendBuffer[0];
	str->sci->SCR3.BYTE |= 0x80; // Wlaczenie przerwania
}

/**
 * Funkcja składająca ramke z trzech liczb szesnastkowych tworzacych ciagly napis zakonczony znakiem nowej linii.
 * @param str wskaźnik na strukturę ramki do uzupełnienia
 * @param sp Set Point
 * @param pv Process Value
 * @param u wartość wymuszenia
 */
void SendValues(UartStruct* str, _UWORD sp, _UWORD pv, _UWORD u)
{
	IntToHex(str->sendBuffer, sp);
	IntToHex(&(str->sendBuffer[4]), pv);
	IntToHex(&(str->sendBuffer[8]), u);
	str->sendBuffer[12] = '\n';
	str->toSend = 13;
	str->sent = 0;
	str->sci->TDR = str->sendBuffer[0];
	str->sci->SCR3.BYTE |= 0x80;
}

void SendInterruptHandler(UartStruct* str)
{
	++(str->sent);
	--(str->toSend);
	if(str->toSend > 0)
	{
		(str->sci)->TDR = str->sendBuffer[str->sent];
	}
	else
	{
		str->sci->SCR3.BYTE &= 0x7F;
	}
}

void pcReceiveHandler(char rcv)
{
	if(rcv != '\n')
	{
		pcConn.receiveBuffer[pcConn.received] = rcv;
		++(pcConn.received);
	}
	else
	{		
		if(pcConn.received == 4)
		{
			pidInst.sp = (float)HexToInt(pcConn.receiveBuffer) / 100.0;
			IO.PDR5.BYTE = 0x01;
		}
		else if(pcConn.received == 12)
		{
			float kp = (float)HexToInt(pcConn.receiveBuffer) / 100.0;
			float ki = (float)HexToInt(&pcConn.receiveBuffer[4]) / 100.0;
			float kd = (float)HexToInt(&pcConn.receiveBuffer[8]) / 100.0;
			PID_UpdateParams(&pidInst, kp, ki, kd);
			
		LCD_Write_String(0, 0, pcConn.receiveBuffer);
		}
		pcConn.received = 0;
	}
}

void objectReceiveHandler(char rcv)
{
	if(rcv != '\n')
	{
		objectConn.receiveBuffer[objectConn.received] = rcv;
		++(objectConn.received);
	}
	else
	{
		IO.PDR5.BYTE ^= 0x02;
		pv = HexToInt(objectConn.receiveBuffer) / 100.0;
		objectConn.received = 0;
	}
}
