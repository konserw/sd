/***********************************************************************/
/*                                                                     */
/*  FILE        :resetprg.c                                            */
/*  DATE        :Fri, Mar 22, 2013                                     */
/*  DESCRIPTION :Reset Program                                         */
/*  CPU TYPE    :H8/36079                                              */
/*                                                                     */
/*  This file is generated by Renesas Project Generator (Ver.4.8).     */
/*                                                                     */
/***********************************************************************/
                  
/**
 * @file resetprg.c
 * This file is generated by Renesas Project Generator (Ver.4.8).
 */

#include	<machine.h>
#include	<_h_c_lib.h>
#include	"typedefine.h"
#include	"stacksct.h"

extern void main(void);
__entry(vect=0) void PowerON_Reset(void);
//__interrupt(vect=1) void Manual_Reset(void);
		
extern void HardwareSetup(void);

#pragma section ResetPRG

__entry(vect=0) void PowerON_Reset(void)
{ 
	 set_imask_ccr((_UBYTE)1);
	_INITSCT();
		
	HardwareSetup();				// Use Hardware Setup
	set_imask_ccr((_UBYTE)0);

	main();

	sleep();
}

//__interrupt(vect=1) void Manual_Reset(void)	// Remove the comment when you use Manual Reset
//{
//} 
