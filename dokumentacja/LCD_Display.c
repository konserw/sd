/*******************************************************************************
 *
 *  Module:         LCD_Dispaly.c
 *
 *  Description:    Module to access the 2 x 16 character LCD-display.
 *
 *  Compiler:       H8S, H8/300 Toolchain Version 6.1.2.0
 *
 *  Processors:     H8/36077,36079 on ModSDKH8-CH8T36077/36079
 *
 *  Author(s):      Maik Otto
 *
 *  Company:        Delphin electronic GmbH, Mühlhausen/Thüringen (Germany)
 *                  info@delphin-electronic.de
 *
 *  $Id: LCD_Display.c 2 2007-08-23 11:15:18Z Maik $
 *
 ******************************************************************************/

#include "LCD_Display.h"
#include "iodefine.h"
#include <machine.h>


// Here we adapt the LCD-disply to the io-mapping.
// This section should be the one and only to adopt your display to
// specific ports - no change in the other code is necessary !

// LCD-Data-Port    is Port10 using all upper 4 bits
// LCD-Control-Port is Port3 using RS on bit2, EN on bit3

#define     CLR_LCD_RS              (IO.PDR3.BIT.B2 = 0)
#define     SET_LCD_RS              (IO.PDR3.BIT.B2 = 1)

#define     CLR_LCD_EN              (IO.PDR3.BIT.B3 = 0)
#define     SET_LCD_EN              (IO.PDR3.BIT.B3 = 1)

#define     LCD_DATA_PORT           (IO.PDR3.BYTE)

#define     LCD_CTRL_PORT           (IO.PDR3.BYTE)


unsigned char PCR3_Value = 0x00;


static void LCD_Wait(void)
{
    volatile unsigned int i = 60;       // do some timing
    while(i--);
}


static void LCD_Do_Strobe(void)
{
    SET_LCD_EN;                     // EN -> 1 -> Strobe rising edge
    nop();
    nop();
    nop();
    nop();
    nop();
    nop();
    nop();
    nop();
    nop();
	nop();
    nop();
    nop();
    nop();
    nop();
    nop();
    nop();
    nop();
    nop();
    CLR_LCD_EN;                     // EN -> 0 -> Strobe falling edge
}


static void LCD_Put_Cmd_Nibble(unsigned char data)
{
    CLR_LCD_RS;                     // RS -> 0 -> Command access

    // provide the lower nibble of the data
    LCD_DATA_PORT = (LCD_DATA_PORT & 0x0F) | ((data & 0x0F) << 4);
    LCD_Do_Strobe();
    LCD_Wait();
}


static void LCD_Put_Byte(unsigned char data)
{
    // provide the upper nibble of the data first
    LCD_DATA_PORT = (LCD_DATA_PORT & 0x0F) | (data & 0xF0);
    LCD_Do_Strobe();

    // provide the lower nibble of the data next
    LCD_DATA_PORT = (LCD_DATA_PORT & 0x0F) | ((data & 0x0F) << 4);
    LCD_Do_Strobe();
    LCD_Wait();
}


static void LCD_Write_Command(unsigned char command)
{
    CLR_LCD_RS;                     // RS -> 0 -> Command access
    LCD_Put_Byte(command);
}


static void LCD_Write_Data(unsigned char data)
{
    SET_LCD_RS;                     // RS -> 0 -> Data access
    LCD_Put_Byte(data);
}


void LCD_Write_String(unsigned char LineNumber, unsigned char StartPos, const unsigned char* Text)
{
    unsigned int i;

    LCD_Write_Command(0x80 + LineNumber*0x40 + StartPos);   // select the position to write into

    for (i=0; i<16; i++) {              // limit writing to 16 chars
        if (*Text == 0) {               // leave write loop if termination
            break;                      // of string was found
        }
        LCD_Write_Data(*Text);          // write a single char to the display
        Text++;                         // move pointer to next position
    }
}


void LCD_Clear_Display(void)
{
    volatile unsigned int cnt = 0x1000;

    LCD_Write_Command(0x01);

    while(--cnt);                       // wait for command completion

}



void LCD_Set_Cursor_Home(void)
{
    volatile unsigned int cnt = 0x1000;

    LCD_Write_Command(0x02);

    while(--cnt);                       // wait for command completion
}



void LCD_Set_Cursor_Mode(_Bool On, _Bool Blinking)
{
    unsigned char Cmd = 0x0C;

    if (On)         Cmd |= 0x02;
    if (Blinking)   Cmd |= 0x01;

    LCD_Write_Command(Cmd);
}


void LCD_Init(void)
{
    volatile unsigned char cnt = 0;

    CLR_LCD_RS;                     // set control lines to low
    CLR_LCD_EN;

    IO.PCR3 = PCR3_Value = PCR3_Value | 0x0C;   // switch control-part-lines for RS, EW to output
    IO.PCR3 = PCR3_Value = PCR3_Value | 0xF0;   // switch data-port-lines to output

                                    // Init-Sequence - for details look at
                                    // datasheet for HD44780-compatible LCD-Display

    LCD_Put_Cmd_Nibble(0x03);       // send this sequence 3 times to init correct
    while(--cnt);
    LCD_Put_Cmd_Nibble(0x03);
    while(--cnt);
    LCD_Put_Cmd_Nibble(0x03);
    while(--cnt);

    LCD_Put_Cmd_Nibble(0x02);       // select 4-bit-interface-mode
    while(--cnt);

    LCD_Write_Command(0x28);        // switch to 4-bit-interface-mode
    while(--cnt);                   // with 2 Lines and 5 x 7 character font

    LCD_Write_Command(0x0C);        // Display On, Cursor Off, Blinking Off
    while(--cnt);

    LCD_Write_Command(0x06);        // Enter Mode, Autoincrement
    while(--cnt);

    LCD_Write_Command(0x14);        // Move Cursor from left to right
    while(--cnt);
}
