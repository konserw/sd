#include "pid.h"

#define _kp 1.0
#define _ki 2.0
#define _kd 0.1
#define Tp 0.5
#define initialSP 0.0;

void PID_Init(PID_Struct* str)
{
	str -> q0 = _kp + (_ki * _kp * Tp) + (_kd * _kp / Tp);
	str -> q1 = - _kp - 2 * _kd * _kp * Tp;
	str -> q2 = _kp;
	str -> tp = Tp;
	str -> err_prev = 0;
	str -> err_prev2 = 0;
	str -> out_prev = 0;
	str -> sp = initialSP;
}

void PID_UpdateParams(PID_Struct* str, float kp, float ki, float kd)
{
	str -> q0 = kp + (ki * kp * Tp) + (kd * kp / Tp);
	str -> q1 = - kp - 2 * kd * kp * Tp;
	str -> q2 = kp;
}

void PID_UpdateSetpoint(PID_Struct* str,  float value)
{
	str -> sp = value;
}

float PID_Sim(PID_Struct* str, float pv)
{
	float err = str -> sp - pv;
	float out;

	out = str -> q0 * err;
	out += str -> q1 * str -> err_prev;
	out += str -> q2 * str -> err_prev2;
	out += str -> out_prev;

	out = (out > 100.0 ? 100.0 : out);
	out = (out < 0 ? 0 : out);

	str -> err_prev2 = str -> err_prev;
	str -> err_prev = err;
	str -> out_prev = out;

	return out;
}
