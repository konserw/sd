Repozytorium projektu SD, w skład którego wczodzą:
Regulator zaimplementowany na mikrokontrolerze hitachi h8
Symulator obiektu dynamicznego zaimplementowany na mikrokontrolerze mpc555
Aplikacja labview na PC prezentująca wykres wartości zadanej i odpowiedzi obiektu oraz umożliwijąca zmianę wartości zadanej regulatora

Połączenia:
obiekt <-> regulator => uart
regulator <-> PC => wirtualny COM pod USB

Format ramki danych przekazywanje przez uart/USB
liczba z zakresu 0-1000 promil zapisana w stringu w formie szesnastkowej
